<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('clients', 'ClientController@show');

Route::get('clients/{reg_id}/remove/', 'ClientController@destroy');

Route::get('clients/{reg_id}/edit/', 'ClientController@edit');

Route::post('clients/{reg_id}/edit/', 'ClientController@update');

Route::post('/clients/add/', 'ClientController@store');

Route::get('/clients/add', function () {
	return view('clients.add');
});

Route::get('orders', 'OrderController@index');

Route::get('orders/add', function () {
	return view('orders.add');
});

Route::post('orders/add/', 'OrderController@store');

Route::get('orders/{order_id}/edit/', 'OrderController@edit');

Route::post('orders/{order_id}/edit/', 'OrderController@update');

Route::get('orders/{order_id}/remove/', 'OrderController@destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
