<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orderList = DB::table('orders')->get();

        return view('orders.main', ['orders' => $orderList]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $validClient = DB::table('clients')->where('reg_id', $req->input('client_id'))->first();

        if ($validClient) {
            $name = $req->input('name');
            $client_id = $req->input('client_id');
            $info = $req->input('info');
            $price = $req->input('price');

            $data = array('name' => $name, 'client_id' => $client_id, 'info' => $info, 'price' => $price);

            DB::table('orders')->insert($data);

            return redirect('orders')->with(['msg' => 'add']);
        } else {
            return redirect('orders/add')->with(['msg' => 'client_id', 'id' => $req->input('client_id')]);
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $orderList = DB::table('orders')->where('order_id', $id)->first();

        return view('orders.edit', ['orders' => $orderList]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        $validClient = DB::table('clients')->where('reg_id', $req->input('client_id'))->first();

        if ($validClient) {
            DB::table('orders')->where('order_id', $id)->update(['name' => $req->input('name'),
                                                               'client_id' => $req->input('client_id'),
                                                               'info' => $req->input('info'),
                                                               'price' => $req->input('price')]);


            return redirect('orders')->with(['msg' => 'edit', 'id' => $req->input('client_id')]);
        } else {
            return redirect('orders/'.$id.'/edit')->with(['msg' => 'client_id', 'id' => $req->input('client_id')]);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($order_id)
    {
        $order = DB::table('orders')->where('order_id', $order_id)->delete();

        return redirect('orders')->with(['msg' => 'delete', 'id' => $order_id]);
    }
}
