<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $req)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $name = $req->input('name');
        $email = $req->input('email');
        $number = $req->input('number');

        $data = array('name' => $name, 'email' => $email, 'number' => $number);

        DB::table('clients')->insert($data);

        return redirect('clients')->with(['msg' => 'add']);
    }

    /**
     * Display all
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $clientData = DB::table('clients')->get();

        return view('clients.main', ['clients' => $clientData]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($reg_id)
    {
        $clientData = DB::table('clients')->where('reg_id', $reg_id)->get();

        return view('clients.edit', ['clients' => $clientData]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $reg_id)
    {
        DB::table('clients')->where('reg_id', $reg_id)->update(['name' => $req->input('name'),
                                                               'email' => $req->input('email'),
                                                               'number' => $req->input('number')]);


        return redirect('clients')->with(['msg' => 'edit']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($reg_id)
    {
        DB::table('clients')->where('reg_id', $reg_id)->delete();
        DB::table('orders')->where('client_id', $reg_id)->delete();

        return redirect('/clients')->with(['msg' => 'delete']);
    }
}
