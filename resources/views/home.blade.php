@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                @guest
                    <div class="panel-heading"><a href="/login">Login</a> or <a href="/register">Register</a> first!</div>
                @else
                    <div class="panel-heading">Sveiks!</div>

                    <div class="panel-body">
                        Apmeklē <a href="clients">"Klientu uzskate"</a> vai <a href="orders">"Klientu pasūtījumi"</a>, lai veiktu kādas darbības!
                    </div>
                @endguest
            </div>
        </div>
    </div>
</div>
@endsection
