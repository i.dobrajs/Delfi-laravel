@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                @guest
                    <div class="panel-heading"><a href="/login">Login</a> or <a href="/register">Register</a> first!</div>
                @else
                    <div class="panel-heading">
                        Rediģēt klientu
                    </div>
                    @if (session()->has('msg'))
                        @if (session()->get('msg') == 'client_id')
                            <div class="panel-body bg-danger text-white">
                                Klients ar id {{ session()->get('id') }} netika atrasts!
                            </div>
                        @endif
                    @endif

                    @if (count($orders) > 0)
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST" action="/orders/{{ $orders->order_id}}/edit">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Nosaukums</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ $orders->name }}" required autofocus>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('client_id') ? ' has-error' : '' }}">
                                <label for="client_id" class="col-md-4 control-label"><a href="/clients" target="_blank" title="Atvērt klientu sarakstu">Klienta</a> id</label>

                                <div class="col-md-6">
                                    <input id="client_id" type="text" class="form-control" name="client_id" value="{{ $orders->client_id }}" required autofocus>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('info') ? ' has-error' : '' }}">
                                <label for="info" class="col-md-4 control-label">Apraksts</label>

                                <div class="col-md-6">
                                    <textarea id="info" type="text" class="form-control" name="info" required autofocus>{{ $orders->info }}</textarea>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('client_id') ? ' has-error' : '' }}">
                                <label for="price" class="col-md-4 control-label">Cena eiro</label>

                                <div class="col-md-6">
                                    <input id="price" type="text" class="form-control" name="price" value="{{ $orders->price }}" required autofocus>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Rediģēt pasūtījumu
                                    </button>
                                </div>
                            </div>
                            </form>                            
                        </div>
                    @else
                        <div class="panel-body bg-danger text-white">Netradām pasūtījumu ar šādu id! Apskatiet tos <a href="../../orders">šeit</a></div>
                    @endif
                @endguest
            </div>
        </div>
    </div>
</div>
@endsection