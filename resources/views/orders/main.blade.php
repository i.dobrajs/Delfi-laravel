@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                @guest
                    <div class="panel-heading"><a href="/login">Login</a> or <a href="/register">Register</a> first!</div>
                @else
                    <div class="panel-heading">
                        Klientu pasūtījumi
                        <a href="orders/add" type="button" class="btn btn-primary add-btn">Add</a>
                    </div>

                    @if (session()->has('msg'))
                        @if (session()->get('msg') == 'delete')
                            <div class="panel-body bg-success text-white">
                                Pasūtījums ar id {{ session()->get('id') }} tika dzēsts!
                            </div>
                        @elseif (session()->get('msg') == 'edit')
                            <div class="panel-body bg-success text-white">
                                Pasūtījums ar id {{ session()->get('id') }} tika rediģēts sekmīgi!
                            </div>
                        @endif
                    @endif

                    @if (count($orders) > 0)
                    <div class="panel-body clients">
                        <table class="table">
                            <tr>
                                <th class="col col-lg-1">#</th>
                                <th>Nosaukums</th>
                                <th>Klienta id</th>
                                <th>Info</th>
                                <th>Cena EUR</th>
                            </tr>
                            @foreach ($orders as $order)
                            <tr>
                                <td class="col col-lg-1">{{ $order->order_id }}</td>
                                <td>{{ $order->name }}</td>
                                <td>{{ $order->client_id }}</td>
                                <td>{{ $order->info }}</td>
                                <td>{{ $order->price }}</td>
                                <td class="btn-box"><a href="orders/{{ $order->order_id }}/edit" type="button" class="btn btn-warning">Edit</a></td>
                                <td class="btn-box"><a href="orders/{{ $order->order_id }}/remove" type="button" class="btn btn-danger">Remove</a></td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                    @else
                        <div class="panel-body">Nav neviena pasūtījuma!</div>
                    @endif
                @endguest
            </div>
        </div>
    </div>
</div>
@endsection