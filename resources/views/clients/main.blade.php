@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                @guest
                    <div class="panel-heading"><a href="/login">Login</a> or <a href="/register">Register</a> first!</div>
                @else
                    <div class="panel-heading">
                        Klientu uzskate
                        <a href="clients/add" type="button" class="btn btn-primary add-btn">Add</a>
                    </div>

                    @if (session()->has('msg'))
                        @if (session()->get('msg') == 'add')
                            <div class="panel-body bg-success text-white">
                                Klients pievienots!
                            </div>
                        @elseif (session()->get('msg') == 'edit')
                            <div class="panel-body bg-success text-white">
                                Klients rediģēts!
                            </div>
                        @elseif (session()->get('msg') == 'delete')
                            <div class="panel-body bg-success text-white">
                                Klients dzēsts!
                            </div>
                        @endif
                    @endif

                    @if (count($clients) > 0)
                    <div class="panel-body clients">
                        <table class="table">
                            <tr>
                                <th class="col col-lg-1">#</th>
                                <th>Nosaukums</th>
                                <th>E-pasts</th>
                                <th>Numurs</th>
                            </tr>
                            @foreach ($clients as $client)
                            <tr>
                                <td class="col col-lg-1">{{ $client->reg_id }}</td>
                                <td>{{ $client->name }}</td>
                                <td>{{ $client->email }}</td>
                                <td>{{ $client->number }}</td>
                                <td class="btn-box"><a href="clients/{{ $client->reg_id }}/edit" type="button" class="btn btn-warning">Edit</a></td>
                                <td class="btn-box"><a href="clients/{{ $client->reg_id }}/remove" type="button" class="btn btn-danger">Remove</a></td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                    @else
                        <div class="panel-body">Nav neviena klienta!</div>
                    @endif
                @endguest
            </div>
        </div>
    </div>
</div>
@endsection