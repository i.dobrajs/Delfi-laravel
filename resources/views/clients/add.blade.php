@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                @guest
                    <div class="panel-heading"><a href="/login">Login</a> or <a href="/register">Register</a> first!</div>
                @else
                    <div class="panel-heading">
                        Pievienot klientu
                    </div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="/clients/add/">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nosaukums</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" required autofocus>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-pasts</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" required autofocus>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="number" class="col-md-4 control-label">Numurs</label>

                            <div class="col-md-6">
                                <input id="number" type="text" class="form-control" name="number" required autofocus>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Pievienot klientu
                                </button>
                            </div>
                        </div>
                        </form>                            
                    </div>
                @endguest
            </div>
        </div>
    </div>
</div>
@endsection